(ns property.core
  (:require [clojure.test.check :as tc]
            [clojure.test.check.generators :as gen]
            [clojure.test.check.properties :as prop]
            [schema.core :as schema]
            [schema.experimental.complete :as schema.complete]
            [schema.experimental.generators :as schema.generators]))


(def sort-is-idempotent-property

  (prop/for-all [v (gen/vector gen/int)]
                (= (sort v)
                   (sort (sort v)))))


(def sorted-first-less-than-last-property

  (prop/for-all [v (gen/not-empty (gen/vector gen/int))]

                (let [s (sort v)]
                  (<= (first s) (last s)))))


(def sort-first-value-is-min-trusted-property

  (prop/for-all [v (gen/not-empty (gen/vector gen/int))]
                (=
                 (apply min v)
                 (first (sort v)))))



(>pprint (tc/quick-check 100 sorted-first-less-than-last-property))

(>pprint (tc/quick-check 100 sort-is-idempotent-property))

(>pprint (tc/quick-check 100 sorted-first-less-than-last-property))





(defn broken-concat [x y]
  (if (and (some odd? x) (some neg? x) (some #(= 100 %) x))
    (concat x y ["oh" "no" "!" "extra" "stuff"])
    (concat x y)))




(def concat-does-concat-stuff-property
  (prop/for-all [vec-1 (gen/vector gen/int)
                 vec-2 (gen/vector gen/int)]
                (=
                 (count (broken-concat vec-1 vec-2))
                 (+ (count vec-1) (count vec-2)))))

(tc/quick-check 100 concat-does-concat-stuff-property)






(defn is-max? [n [x y]]
  (and (or (= n x)
           (= n y))
       (and (>= n x)
            (>= n y))))

(def test-max-property
  (prop/for-all [ints (gen/vector gen/int 2)]
                (is-max? (apply max ints) ints)))



(tc/quick-check 1 test-max-property)


(def reverse-is-symetrical-property
  (prop/for-all [v (gen/vector gen/any)]
                (= v
                   (reverse (reverse v)))))


(tc/quick-check 100 reverse-is-symetrical-property)



(>pprint (gen/sample five-through-nine))




(def languages (gen/elements ["clojure" "haskell" "erlang" "scala" "python"]))

(>pprint (gen/sample languages))




(def int-or-nil (gen/one-of [gen/int (gen/return nil)]))
(>pprint (gen/sample int-or-nil))




(>pprint (gen/sample (gen/frequency [[9 gen/int] [1 (gen/return nil)]])))




(>pprint (gen/sample (gen/large-integer* {:min 10000 :max 20000})))

(>pprint (gen/sample gen/ratio))

(>pprint (gen/sample gen/any-printable))



(def day-generator (gen/frequency [[5 (gen/elements [:monday :tuesday :wednesday :thursday :friday])]
                                   [20 (gen/elements [:saturday :sunday])]]))

(>pprint (gen/sample day-generator))







(>pprint (gen/sample (gen/tuple gen/int gen/string-alphanumeric)))

(>pprint (gen/sample (gen/tuple gen/int day-generator)))



(defrecord User [user-name user-id email active?])

(def domain (gen/elements ["gmail.com" "hotmail.com" "computer.org"]))


(def email-gen
  (gen/fmap (fn [[name domain-name]]
              (str name "@" domain-name))
            (gen/tuple (gen/not-empty gen/string-alphanumeric) domain)))



(def user-gen
  (gen/fmap (partial apply ->User)
            (gen/tuple (gen/not-empty gen/string-alphanumeric)
                       gen/nat
                       email-gen
                       gen/boolean)))


(>pprint (last (gen/sample email-gen)))

(>pprint (last (gen/sample user-gen)))
